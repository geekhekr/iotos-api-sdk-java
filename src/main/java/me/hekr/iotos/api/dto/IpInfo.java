package me.hekr.iotos.api.dto;

import java.io.Serializable;

public class IpInfo implements Serializable {
  private static final long serialVersionUID = -8527104618122701513L;
  private String ip;
  private String country;
  private String province;
  private String city;

  public IpInfo(String ip) {
    this.ip = ip;
  }

  public String getIp() {
    return this.ip;
  }

  public String getCountry() {
    return this.country;
  }

  public String getProvince() {
    return this.province;
  }

  public String getCity() {
    return this.city;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "IpInfo(ip="
        + this.getIp()
        + ", country="
        + this.getCountry()
        + ", province="
        + this.getProvince()
        + ", city="
        + this.getCity()
        + ")";
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof IpInfo)) {
      return false;
    } else {
      IpInfo other = (IpInfo) o;
      if (!other.canEqual(this)) {
        return false;
      } else {
        Object this$ip = this.getIp();
        Object other$ip = other.getIp();
        if (this$ip == null) {
          if (other$ip != null) {
            return false;
          }
        } else if (!this$ip.equals(other$ip)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof IpInfo;
  }

  public IpInfo() {}
}
