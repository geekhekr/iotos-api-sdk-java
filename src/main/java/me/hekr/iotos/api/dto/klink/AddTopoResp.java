package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;
import me.hekr.iotos.api.enums.ErrorCode;

public class AddTopoResp extends KlinkResp {
  private Dev sub;

  public AddTopoResp(ErrorCode errorCode, String desc) {
    super(errorCode, desc);
  }

  public AddTopoResp(ErrorCode errorCode) {
    super(errorCode);
  }

  @Override
  public String getAction() {
    return Action.ADD_TOPO_RESP.getAction();
  }

  public Dev getSub() {
    return this.sub;
  }

  public void setSub(Dev sub) {
    this.sub = sub;
  }

  @Override
  public String toString() {
    return "AddTopoResp(super=" + super.toString() + ", sub=" + this.getSub() + ")";
  }

  public AddTopoResp() {}
}
