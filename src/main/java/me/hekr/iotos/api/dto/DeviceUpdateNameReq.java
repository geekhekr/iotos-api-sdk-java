package me.hekr.iotos.api.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 添加设备请求 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DeviceUpdateNameReq extends DeviceAddReq {}
