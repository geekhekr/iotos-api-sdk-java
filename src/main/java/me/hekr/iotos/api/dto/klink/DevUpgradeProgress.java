package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class DevUpgradeProgress extends KlinkDev {
  private static final long serialVersionUID = 9016217483636057893L;
  private int progress;
  private String type;

  @Override
  public String getAction() {
    return Action.DEV_UPGRADE_PROGRESS.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof DevUpgradeProgress)) {
      return false;
    } else {
      DevUpgradeProgress other = (DevUpgradeProgress) o;
      if (!other.canEqual(this)) {
        return false;
      } else if (!super.equals(o)) {
        return false;
      } else if (this.getProgress() != other.getProgress()) {
        return false;
      } else {
        Object this$type = this.getType();
        Object other$type = other.getType();
        if (this$type == null) {
          if (other$type == null) {
            return true;
          }
        } else if (this$type.equals(other$type)) {
          return true;
        }

        return false;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof DevUpgradeProgress;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = result * 59 + this.getProgress();
    Object $type = this.getType();
    result = result * 59 + ($type == null ? 43 : $type.hashCode());
    return result;
  }

  public DevUpgradeProgress() {}

  public int getProgress() {
    return this.progress;
  }

  public String getType() {
    return this.type;
  }

  public void setProgress(int progress) {
    this.progress = progress;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "DevUpgradeProgress(progress=" + this.getProgress() + ", type=" + this.getType() + ")";
  }
}
