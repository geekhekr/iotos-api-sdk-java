package me.hekr.iotos.api.dto;

import java.util.Date;


public class CloudSendMsgDTO {
  private String messageId;
  /*错误码*/
  private int code;
  /*描述*/
  private String desc;
  /*消息创建时间*/
  private Date createTime;

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}
