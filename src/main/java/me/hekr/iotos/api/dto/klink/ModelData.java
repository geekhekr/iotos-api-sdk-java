package me.hekr.iotos.api.dto.klink;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

public class ModelData implements Serializable {
  private static final long serialVersionUID = 5838451843005203760L;
  private String cmd;

  @JsonInclude(Include.NON_NULL)
  private Map<String, Object> params;

  public Map<String, Object> getParams() {
    return this.params == null ? Collections.emptyMap() : this.params;
  }

  public ModelData() {}

  public String getCmd() {
    return this.cmd;
  }

  public void setCmd(String cmd) {
    this.cmd = cmd;
  }

  public void setParams(Map<String, Object> params) {
    this.params = params;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof ModelData)) {
      return false;
    } else {
      ModelData other = (ModelData) o;
      if (!other.canEqual(this)) {
        return false;
      } else {
        Object this$cmd = this.getCmd();
        Object other$cmd = other.getCmd();
        if (this$cmd == null) {
          if (other$cmd != null) {
            return false;
          }
        } else if (!this$cmd.equals(other$cmd)) {
          return false;
        }

        Object this$params = this.getParams();
        Object other$params = other.getParams();
        if (this$params == null) {
          if (other$params != null) {
            return false;
          }
        } else if (!this$params.equals(other$params)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof ModelData;
  }

  @Override
  public String toString() {
    return "ModelData(cmd=" + this.getCmd() + ", params=" + this.getParams() + ")";
  }
}
