package me.hekr.iotos.api.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/** 添加设备请求 */
@Data
public class DeviceAddReq {

  /** 设备名字 */
  private String name;
  /** pk 必填 */
  private String pk;
  /** 设备ID 必填 */
  private String devId;

  /** 设备地址 */
  @NotEmpty(message = "设备地址不能为空")
  private String address;

  /** 所属镇街 */
  @NotEmpty(message = "所属镇街不能为空")
  private String town;

  /** 所属村社 */
  @NotEmpty(message = "所属村社不能为空")
  private String village;

  /** 经纬度 */
  @NotEmpty(message = "经纬度不能为空")
  private Double[] location;

  /** z轴 */
  @NotEmpty(message = "z轴不能为空")
  private String zaxis;
}
