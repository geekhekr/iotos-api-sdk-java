package me.hekr.iotos.api.dto;

import lombok.Data;

@Data
public class UploadResult {
  protected String url;
  protected String filename;
  protected String md5;
  /** 文件大小 */
  protected long bytes;

  protected String path;
}
