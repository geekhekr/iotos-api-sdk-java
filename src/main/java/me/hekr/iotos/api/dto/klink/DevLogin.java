package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class DevLogin extends KlinkDev {
  private String random;
  private String hashMethod;
  private String sign;

  public DevLogin() {}

  @Override
  public String getAction() {
    return Action.DEV_LOGIN.getAction();
  }

  public String getRandom() {
    return this.random;
  }

  public String getHashMethod() {
    return this.hashMethod;
  }

  public String getSign() {
    return this.sign;
  }

  public void setRandom(String random) {
    this.random = random;
  }

  public void setHashMethod(String hashMethod) {
    this.hashMethod = hashMethod;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  @Override
  public String toString() {
    return "DevLogin(super="
        + super.toString()
        + ", random="
        + this.getRandom()
        + ", hashMethod="
        + this.getHashMethod()
        + ", sign="
        + this.getSign()
        + ")";
  }
}
