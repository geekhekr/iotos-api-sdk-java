package me.hekr.iotos.api.dto;

import java.io.Serializable;
import org.jetbrains.annotations.NotNull;

public class Tag implements Serializable, Comparable<Tag> {
  private static final long serialVersionUID = -2216750312738203195L;
  private String key;
  private String value;

  public static Tag of(String key, String value) {
    return new Tag(key, value);
  }

  public String getKey() {
    return this.key;
  }

  public String getValue() {
    return this.value;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Tag(String key, String value) {
    this.key = key;
    this.value = value;
  }

  public Tag() {}

  @Override
  public int compareTo(@NotNull Tag o) {
    if (this.key == null || o.key == null) {
      return 0;
    }
    return this.key.compareTo(o.key);
  }
}
