package me.hekr.iotos.api.dto;

import java.io.Serializable;
import me.hekr.iotos.api.dto.klink.ModelData;

public class Snapshot implements Serializable {
  private static final long serialVersionUID = -9064767418611128865L;
  private String pk;
  private String devId;
  private long timestamp;
  private ModelData data;

  public Snapshot(String pk, String devId) {
    this.pk = pk;
    this.devId = devId;
  }

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public long getTimestamp() {
    return this.timestamp;
  }

  public ModelData getData() {
    return this.data;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public void setData(ModelData data) {
    this.data = data;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof Snapshot)) {
      return false;
    } else {
      Snapshot other = (Snapshot) o;
      if (!other.canEqual(this)) {
        return false;
      } else {
        Object this$pk = this.getPk();
        Object other$pk = other.getPk();
        if (this$pk == null) {
          if (other$pk != null) {
            return false;
          }
        } else if (!this$pk.equals(other$pk)) {
          return false;
        }

        Object this$devId = this.getDevId();
        Object other$devId = other.getDevId();
        if (this$devId == null) {
          if (other$devId != null) {
            return false;
          }
        } else if (!this$devId.equals(other$devId)) {
          return false;
        }

        if (this.getTimestamp() != other.getTimestamp()) {
          return false;
        } else {
          Object this$data = this.getData();
          Object other$data = other.getData();
          if (this$data == null) {
            if (other$data != null) {
              return false;
            }
          } else if (!this$data.equals(other$data)) {
            return false;
          }

          return true;
        }
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof Snapshot;
  }

  @Override
  public String toString() {
    return "Snapshot(pk="
        + this.getPk()
        + ", devId="
        + this.getDevId()
        + ", timestamp="
        + this.getTimestamp()
        + ", data="
        + this.getData()
        + ")";
  }

  public Snapshot() {}

  public Snapshot(String pk, String devId, long timestamp, ModelData data) {
    this.pk = pk;
    this.devId = devId;
    this.timestamp = timestamp;
    this.data = data;
  }
}
