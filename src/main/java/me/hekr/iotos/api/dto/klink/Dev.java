package me.hekr.iotos.api.dto.klink;

import java.io.Serializable;

public class Dev implements Serializable {
  private static final long serialVersionUID = -3682121569498210336L;
  private String pk;
  private String devId;

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }
}
