package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import me.hekr.iotos.api.enums.CheckType;
import me.hekr.iotos.api.enums.DataType;

@JsonTypeName("LENGTH")
public class DataValueLengthChecker implements DataValueChecker {
  private ModelParamValue<Long> max;
  private ModelParamValue<Long> min;

  @Override
  public CheckType getType() {
    return CheckType.LENGTH;
  }

  @Override
  public boolean isValid(DataType dataType, Object value) {
    if (value == null) {
      return false;
    } else {
      DataType datatype = DataType.fromValue(value);
      boolean isTypeValid = datatype == DataType.STRING;
      if (!isTypeValid) {
        return false;
      } else {
        String val = (String) value;
        return (long) val.length() >= (Long) this.min.getValue()
            && (long) val.length() <= (Long) this.max.getValue();
      }
    }
  }

  @Override
  public boolean canConvertTo(DataType dataType) {
    return true;
  }

  @Override
  public boolean valid(DataType dataType) {
    return this.max != null
        && this.min != null
        && this.max.getValue() != null
        && this.min.getValue() != null;
  }

  @Override
  public String toString() {
    return "length check, length range: [" + this.min.getValue() + ", " + this.max.getValue() + "]";
  }

  public DataValueLengthChecker() {}

  public ModelParamValue<Long> getMax() {
    return this.max;
  }

  public ModelParamValue<Long> getMin() {
    return this.min;
  }

  public void setMax(ModelParamValue<Long> max) {
    this.max = max;
  }

  public void setMin(ModelParamValue<Long> min) {
    this.min = min;
  }
}
