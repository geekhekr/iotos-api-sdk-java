package me.hekr.iotos.api.dto.klink;

import java.util.List;
import me.hekr.iotos.api.enums.Action;

public class BatchDevSend extends KlinkDev {
  private List<SuModelData> data;

  public BatchDevSend() {}

  @Override
  public String getAction() {
    return Action.BATCH_DEV_SEND.getAction();
  }

  public List<SuModelData> getData() {
    return this.data;
  }

  public void setData(List<SuModelData> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "BatchDevSend(super=" + super.toString() + ", data=" + this.getData() + ")";
  }
}
