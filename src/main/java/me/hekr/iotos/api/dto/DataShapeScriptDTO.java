package me.hekr.iotos.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@NoArgsConstructor
public class DataShapeScriptDTO {

  /** 对应的产品，必填 */
  protected String pk;

  @NotBlank protected String script;
  /** 创建时间 */
  protected Date createdDate;
  /** 更新时间 */
  protected Date updatedDate;

  protected boolean enable;
}
