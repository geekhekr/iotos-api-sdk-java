package me.hekr.iotos.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import me.hekr.iotos.api.enums.DataCheckMode;
import me.hekr.iotos.api.enums.DataFormat;
import me.hekr.iotos.api.enums.DeviceLinkType;
import me.hekr.iotos.api.enums.TransferType;

@Getter
@Setter
public class ConsoleProductReq extends ProductDTO {

  @NotBlank(message = "名称不能为空")
  @Size(min = 2, max = 24, message = "名称长度限定2-24个字符")
  private String name;

  @NotBlank(message = "品类不能为空")
  private String cid;

  /** 联网方式 */
  @NotEmpty
  private DeviceLinkType deviceLinkType;

  /** 数据传输类型 */
  @NotEmpty
  private TransferType transferType;

  /** payload 业务数据格式 */
  @NotEmpty
  private DataFormat dataFormat;

  /** 协议参数校验模式 */
  @NotEmpty
  private DataCheckMode dataCheckMode;

  @Size(max = 24, message = "型号限定最多24个字符")
  private String model;

  @Size(max = 24, message = "品牌限定最多24个字符")
  private String brand;

  /** 是否公开 */
  private boolean open;

  private boolean hide;

  private String remark;

  /** 数据来源部门 */
  private String dept;

  /** 省主题 */
  private String provinceTopic;

  /** 区主题 */
  private String districtTopic;

  /** 行业分类 */
  private String industry;

  /** 数据级别 */
  private String dataLevel;

  /** 共享属性 */
  private String shareProp;

  /** 开放属性 */
  private String openProp;

  /** 资源范围描述 */
  private String scopeDesc;

  /** 信息资源描述 */
  private String desc;

  /** 周期 */
  private String period;

  /** 更新时间 */
  private String updateTime;

  /** 所属业务系统 */
  private String businessSys;
}
