package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DevicePacketMeasurement {
  private String pk;
  private String devId;
  private String key;
  private float value;
  private long timestamp;
}
