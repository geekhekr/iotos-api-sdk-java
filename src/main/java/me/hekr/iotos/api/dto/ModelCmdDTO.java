package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.TreeSet;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.hekr.iotos.api.enums.FrameType;

@Data
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class ModelCmdDTO implements Serializable {
  private static final long serialVersionUID = -6040947199844685210L;
  protected String name;
  protected String cmd;
  protected FrameType frameType;
  protected TreeSet<String> params;
  protected String desc;
  protected TreeSet<Tag> tags;
}
