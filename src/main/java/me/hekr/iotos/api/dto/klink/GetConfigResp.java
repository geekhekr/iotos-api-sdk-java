package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;
import me.hekr.iotos.api.enums.ErrorCode;

public class GetConfigResp extends KlinkResp {
  private static final long serialVersionUID = 3722041793170943339L;
  private String url;
  private String md5;

  public GetConfigResp(ErrorCode errorCode) {
    super(errorCode);
  }

  @Override
  public String getAction() {
    return Action.GET_CONFIG_RESP.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof GetConfigResp)) {
      return false;
    } else {
      GetConfigResp other = (GetConfigResp) o;
      if (!other.canEqual(this)) {
        return false;
      } else if (!super.equals(o)) {
        return false;
      } else {
        Object this$url = this.getUrl();
        Object other$url = other.getUrl();
        if (this$url == null) {
          if (other$url != null) {
            return false;
          }
        } else if (!this$url.equals(other$url)) {
          return false;
        }

        Object this$md5 = this.getMd5();
        Object other$md5 = other.getMd5();
        if (this$md5 == null) {
          if (other$md5 != null) {
            return false;
          }
        } else if (!this$md5.equals(other$md5)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof GetConfigResp;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    Object $url = this.getUrl();
    result = result * 59 + ($url == null ? 43 : $url.hashCode());
    Object $md5 = this.getMd5();
    result = result * 59 + ($md5 == null ? 43 : $md5.hashCode());
    return result;
  }

  public String getUrl() {
    return this.url;
  }

  public String getMd5() {
    return this.md5;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setMd5(String md5) {
    this.md5 = md5;
  }

  @Override
  public String toString() {
    return "GetConfigResp(super="
        + super.toString()
        + ", url="
        + this.getUrl()
        + ", md5="
        + this.getMd5()
        + ")";
  }

  public GetConfigResp() {}
}
