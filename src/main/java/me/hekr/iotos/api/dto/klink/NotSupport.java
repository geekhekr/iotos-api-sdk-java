package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class NotSupport extends KlinkDev {
  public NotSupport() {}

  @Override
  public String getAction() {
    return Action.NOT_SUPPORT.getAction();
  }

  @Override
  public String toString() {
    return "NotSupport(super=" + super.toString() + ")";
  }
}
