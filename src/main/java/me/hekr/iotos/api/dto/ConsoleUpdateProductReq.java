package me.hekr.iotos.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: zeyang.li
 * @create: 2019-09-05 修改产品仅支持名称和品类
 */
@Getter
@Setter
public class ConsoleUpdateProductReq {

  @NotBlank(message = "PK不能为空")
  private String pk;

  @NotBlank(message = "名称不能为空")
  @Size(min = 2, max = 24, message = "名称长度限定2-24个字符")
  private String name;

  @NotBlank(message = "品类不能为空")
  private String cid;

  @Size(max = 24, message = "型号限定最多24个字符")
  private String model;

  @Size(max = 24, message = "品牌限定最多24个字符")
  private String brand;

  /** true 公开（可以被其他账户看到并克隆产品属性），false 私有 */
  private boolean open;

  private boolean hide;

  private String remark;

  /** 数据来源部门 */
  private String dept;

  /** 省主题 */
  private String provinceTopic;

  /** 区主题 */
  private String districtTopic;

  /** 行业分类 */
  private String industry;

  /** 数据级别 */
  private String dataLevel;

  /** 共享属性 */
  private String shareProp;

  /** 资源领域 */
  private String scope;

  /** 开放属性 */
  private String openProp;

  /** 资源范围描述 */
  private String scopeDesc;

  /** 信息资源描述 */
  private String desc;

  /** 周期 */
  private String period;

  /** 更新时间 */
  private String updateTime;

  /** 业务系统编码 */
  private String businessCode;

  /** 所属业务系统 */
  private String businessSys;
}
