package me.hekr.iotos.api.dto;

import java.io.Serializable;
import lombok.Data;


@Data
public class LoginRsp implements Serializable {

  private static final long serialVersionUID = 4021035770683221680L;
  private String access_token;
  private String refresh_token;
  private Long expire;
  private String token_type = "bearer";
  private String uid;
}
