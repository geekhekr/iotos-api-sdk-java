package me.hekr.iotos.api.dto.klink;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;

@JsonInclude(Include.NON_NULL)
public class Klink implements Serializable {
  @JsonIgnore public static final String CMD = "cmd";
  private static final long serialVersionUID = -4341021820638489039L;
  protected String action;
  protected long msgId;
  protected String sysCustomRaw;

  public Klink() {}

  public String getAction() {
    return this.action;
  }

  public long getMsgId() {
    return this.msgId;
  }

  public String getSysCustomRaw() {
    return this.sysCustomRaw;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public void setMsgId(long msgId) {
    this.msgId = msgId;
  }

  public void setSysCustomRaw(String sysCustomRaw) {
    this.sysCustomRaw = sysCustomRaw;
  }

  @Override
  public String toString() {
    return "Klink(action="
        + this.getAction()
        + ", msgId="
        + this.getMsgId()
        + ", sysCustomRaw="
        + this.getSysCustomRaw()
        + ")";
  }
}
