package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.TreeSet;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.hekr.iotos.api.enums.DataType;
import me.hekr.iotos.api.enums.FrameType;

@Data
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
public class ModelParamDTO implements Serializable {
  private static final long serialVersionUID = 5644859178187540679L;

  protected String name;

  protected String param;
  protected DataType dataType;

  protected FrameType frameType;

  protected DataValueChecker checkType;

  protected String unit;

  protected String desc;
  protected TreeSet<Tag> tags;
}
