package me.hekr.iotos.api.dto.klink;

import lombok.*;
import me.hekr.iotos.api.enums.Action;
import me.hekr.iotos.api.enums.ErrorCode;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class BatchAddTopoResp extends KlinkResp {

  /** 添加错误的子设备信息，如果code为0，这个为空 */
  private List<AddTopoResp> errSubs;

  public BatchAddTopoResp(ErrorCode errorCode, String desc) {
    super(errorCode, desc);
  }

  public BatchAddTopoResp(ErrorCode errorCode) {
    super(errorCode);
  }

  @Override
  public String getAction() {
    return Action.BATCH_ADD_TOPO_RESP.getAction();
  }
}
