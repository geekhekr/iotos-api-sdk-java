package me.hekr.iotos.api.dto;

import java.io.Serializable;

public class ModelParamValue<T> implements Serializable {
  private static final long serialVersionUID = 8577717179885559003L;
  private T value;
  private String desc;

  public ModelParamValue(T value) {
    this.value = value;
  }

  public T getValue() {
    return this.value;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setValue(T value) {
    this.value = value;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  @Override
  public String toString() {
    return "ModelParamValue(value=" + this.getValue() + ", desc=" + this.getDesc() + ")";
  }

  public ModelParamValue() {}

  public ModelParamValue(T value, String desc) {
    this.value = value;
    this.desc = desc;
  }
}
