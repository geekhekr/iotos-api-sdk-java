package me.hekr.iotos.api.dto;

import lombok.Data;

@Data
public class AggDataQuery {

  /** 设备id 必填 */
  String devId;
  /** 可选 */
  /** pk 必填 */
  private String pk;
  /** 必填 */
  private long startTime;
  /** 必填 */
  private long endTime;
  /** 参数 必填 */
  private String key;
  /**
   * 聚合力度 必填
   *
   * <p>span可选值为5min、1hour和1day，对应的最大时间范围为一周、一个月和一年。 即，当span值选择为5min时，startTime和endTime差值不能大于一周。
   */
  private String span;
}
