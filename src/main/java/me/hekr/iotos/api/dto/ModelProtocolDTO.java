package me.hekr.iotos.api.dto;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ModelProtocolDTO implements Serializable {
  private static final long serialVersionUID = -2739480258311156282L;
  protected String pk;
  protected List<ModelParamDTO> params;
  protected List<ModelCmdDTO> cmds;

  protected Date createdDate;

  protected Date updatedDate;

  public ModelProtocolDTO() {}

  public String getPk() {
    return this.pk;
  }

  public List<ModelParamDTO> getParams() {
    return this.params;
  }

  public List<ModelCmdDTO> getCmds() {
    return this.cmds;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public Date getUpdatedDate() {
    return this.updatedDate;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setParams(List<ModelParamDTO> params) {
    this.params = params;
  }

  public void setCmds(List<ModelCmdDTO> cmds) {
    this.cmds = cmds;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }
}
