package me.hekr.iotos.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceAggPacket {
  /** 时间戳 */
  private long timestamp;
  /** 数值 */
  private double value;
}
