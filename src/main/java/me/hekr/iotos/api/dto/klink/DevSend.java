package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class DevSend extends KlinkDev {
  private ModelData data;

  public DevSend() {}

  @Override
  public String getAction() {
    return Action.DEV_SEND.getAction();
  }

  public ModelData getData() {
    return this.data;
  }

  public void setData(ModelData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "DevSend(super=" + super.toString() + ", data=" + this.getData() + ")";
  }
}
