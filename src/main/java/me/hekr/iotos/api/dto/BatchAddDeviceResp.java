package me.hekr.iotos.api.dto;

import lombok.Data;

@Data
public class BatchAddDeviceResp {
  private String batchName;
  private String batchNumber;
}
