package me.hekr.iotos.api.dto;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

/** 请求为由devId列表组成 */
@Data
public class DevIdListReq {
  @NotEmpty(message = "pk不能为空")
  String pk;

  private List<String> devIds;
}
