package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import me.hekr.iotos.api.enums.DeviceType;
import me.hekr.iotos.api.enums.MessageSourceType;

public class DeviceDTO implements Serializable {
  private static final long serialVersionUID = 1546822448972991937L;

  protected String id;
  protected String finger;
  protected String sessionId;
  protected MessageSourceType messageSourceType;

  @JsonInclude(Include.NON_NULL)
  protected String rootPk;

  @JsonInclude(Include.NON_NULL)
  protected String name;

  @JsonInclude(Include.NON_NULL)
  protected String rootDevId;

  @JsonInclude(Include.NON_NULL)
  protected String parentPk;

  @JsonInclude(Include.NON_NULL)
  protected String parentDevId;

  protected Date createdDate;

  protected Date updatedDate;
  protected Boolean enable;
  protected String pk;
  protected String devId;
  protected boolean online;
  protected String devSecret;
  protected Date loginTime;
  protected Date logoutTime;
  protected Date registerTime;
  protected Date firstLoginTime;
  protected DeviceType deviceType;
  protected List<Tag> tags;
  protected IpInfo ipInfo;
  protected String moduleVersion;
  protected String mcuVersion;
  protected String configVersion;
  protected String batchName;
  protected String imei;

  /** 直接子设备数量 */
  private long subCount;

  public long getSubCount() {
    return subCount;
  }

  public void setSubCount(long subCount) {
    this.subCount = subCount;
  }

  @JsonInclude(Include.NON_NULL)
  protected Boolean gatewayOnline;

  public boolean isEnable() {
    return !Boolean.FALSE.equals(this.enable);
  }

  public Boolean getEnable() {
    return this.isEnable();
  }

  public DeviceDTO() {}

  public String getId() {
    return this.id;
  }

  public String getFinger() {
    return this.finger;
  }

  public String getSessionId() {
    return this.sessionId;
  }

  public MessageSourceType getMessageSourceType() {
    return this.messageSourceType;
  }

  public String getRootPk() {
    return this.rootPk;
  }

  public String getName() {
    return this.name;
  }

  public String getRootDevId() {
    return this.rootDevId;
  }

  public String getParentPk() {
    return this.parentPk;
  }

  public String getParentDevId() {
    return this.parentDevId;
  }

  public Date getCreatedDate() {
    return this.createdDate;
  }

  public Date getUpdatedDate() {
    return this.updatedDate;
  }

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public boolean isOnline() {
    return this.online;
  }

  public String getDevSecret() {
    return this.devSecret;
  }

  public Date getLoginTime() {
    return this.loginTime;
  }

  public Date getLogoutTime() {
    return this.logoutTime;
  }

  public Date getRegisterTime() {
    return this.registerTime;
  }

  public Date getFirstLoginTime() {
    return this.firstLoginTime;
  }

  public DeviceType getDeviceType() {
    return this.deviceType;
  }

  public List<Tag> getTags() {
    return this.tags;
  }

  public IpInfo getIpInfo() {
    return this.ipInfo;
  }

  public String getModuleVersion() {
    return this.moduleVersion;
  }

  public String getMcuVersion() {
    return this.mcuVersion;
  }

  public String getConfigVersion() {
    return this.configVersion;
  }

  public String getBatchName() {
    return this.batchName;
  }

  public String getImei() {
    return this.imei;
  }

  public Boolean getGatewayOnline() {
    return this.gatewayOnline;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setFinger(String finger) {
    this.finger = finger;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public void setMessageSourceType(MessageSourceType messageSourceType) {
    this.messageSourceType = messageSourceType;
  }

  public void setRootPk(String rootPk) {
    this.rootPk = rootPk;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setRootDevId(String rootDevId) {
    this.rootDevId = rootDevId;
  }

  public void setParentPk(String parentPk) {
    this.parentPk = parentPk;
  }

  public void setParentDevId(String parentDevId) {
    this.parentDevId = parentDevId;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }

  public void setOnline(boolean online) {
    this.online = online;
  }

  public void setDevSecret(String devSecret) {
    this.devSecret = devSecret;
  }

  public void setLoginTime(Date loginTime) {
    this.loginTime = loginTime;
  }

  public void setLogoutTime(Date logoutTime) {
    this.logoutTime = logoutTime;
  }

  public void setRegisterTime(Date registerTime) {
    this.registerTime = registerTime;
  }

  public void setFirstLoginTime(Date firstLoginTime) {
    this.firstLoginTime = firstLoginTime;
  }

  public void setDeviceType(DeviceType deviceType) {
    this.deviceType = deviceType;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  public void setIpInfo(IpInfo ipInfo) {
    this.ipInfo = ipInfo;
  }

  public void setModuleVersion(String moduleVersion) {
    this.moduleVersion = moduleVersion;
  }

  public void setMcuVersion(String mcuVersion) {
    this.mcuVersion = mcuVersion;
  }

  public void setConfigVersion(String configVersion) {
    this.configVersion = configVersion;
  }

  public void setBatchName(String batchName) {
    this.batchName = batchName;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }

  public void setGatewayOnline(Boolean gatewayOnline) {
    this.gatewayOnline = gatewayOnline;
  }

  @Override
  public String toString() {
    return "DeviceDTO(id="
        + this.getId()
        + ", finger="
        + this.getFinger()
        + ", sessionId="
        + this.getSessionId()
        + ", messageSourceType="
        + this.getMessageSourceType()
        + ", rootPk="
        + this.getRootPk()
        + ", name="
        + this.getName()
        + ", rootDevId="
        + this.getRootDevId()
        + ", parentPk="
        + this.getParentPk()
        + ", parentDevId="
        + this.getParentDevId()
        + ", createdDate="
        + this.getCreatedDate()
        + ", updatedDate="
        + this.getUpdatedDate()
        + ", enable="
        + this.getEnable()
        + ", pk="
        + this.getPk()
        + ", devId="
        + this.getDevId()
        + ", online="
        + this.isOnline()
        + ", devSecret="
        + this.getDevSecret()
        + ", loginTime="
        + this.getLoginTime()
        + ", logoutTime="
        + this.getLogoutTime()
        + ", registerTime="
        + this.getRegisterTime()
        + ", firstLoginTime="
        + this.getFirstLoginTime()
        + ", deviceType="
        + this.getDeviceType()
        + ", tags="
        + this.getTags()
        + ", ipInfo="
        + this.getIpInfo()
        + ", moduleVersion="
        + this.getModuleVersion()
        + ", mcuVersion="
        + this.getMcuVersion()
        + ", configVersion="
        + this.getConfigVersion()
        + ", batchName="
        + this.getBatchName()
        + ", imei="
        + this.getImei()
        + ", gatewayOnline="
        + this.getGatewayOnline()
        + ")";
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof DeviceDTO)) {
      return false;
    } else {
      DeviceDTO other = (DeviceDTO) o;
      if (!other.canEqual(this)) {
        return false;
      } else {
        Object this$pk = this.getPk();
        Object other$pk = other.getPk();
        if (this$pk == null) {
          if (other$pk != null) {
            return false;
          }
        } else if (!this$pk.equals(other$pk)) {
          return false;
        }

        Object this$devId = this.getDevId();
        Object other$devId = other.getDevId();
        if (this$devId == null) {
          if (other$devId != null) {
            return false;
          }
        } else if (!this$devId.equals(other$devId)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof DeviceDTO;
  }
}
