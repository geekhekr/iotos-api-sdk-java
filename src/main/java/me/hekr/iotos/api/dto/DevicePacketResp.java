package me.hekr.iotos.api.dto;

import lombok.Data;

/** @author du */
@Data
public class DevicePacketResp {
  private String pk;
  private String devId;
  /** 请求 的原始数据 */
  private String rawRequest;
  /** 解码后的数据或者编码前的数据 */
  private String payload;
  /** response编码 */
  private String rawResponse;

  private String frameType;
  private String action;
  private long timestamp;
  private String messageId;
  private IpInfo ipInfo;
  private String request;
  private String response;
}
