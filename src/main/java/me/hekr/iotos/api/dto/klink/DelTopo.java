package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class DelTopo extends KlinkDev {
  private TopoSub sub;

  @Override
  public String getAction() {
    return Action.DEL_TOPO.getAction();
  }

  public TopoSub getSub() {
    return this.sub;
  }

  public void setSub(TopoSub sub) {
    this.sub = sub;
  }

  @Override
  public String toString() {
    return "DelTopo(super=" + super.toString() + ", sub=" + this.getSub() + ")";
  }

  public DelTopo() {}
}
