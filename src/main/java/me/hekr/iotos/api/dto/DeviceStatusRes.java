package me.hekr.iotos.api.dto;

import java.util.Date;
import lombok.Data;

@Data
public class DeviceStatusRes {
  protected String pk;
  protected String devId;
  protected boolean online;
  /* 最近登录时间*/
  protected Date loginTime;
}
