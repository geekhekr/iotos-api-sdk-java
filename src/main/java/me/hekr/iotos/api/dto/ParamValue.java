package me.hekr.iotos.api.dto;

import lombok.Data;

@Data
public class ParamValue {
  private String key;
  private double value;
  private long timestamp;
}
