package me.hekr.iotos.api.dto;

import lombok.Data;
import me.hekr.iotos.api.enums.Action;

@Data
public class HistoryDataQuery {

  /** 设备id 必填 */
  String devId;
  /** 从0开始，必填 */
  int page;
  /** 必填 */
  int size;
  /** 可选 */
  Action action;
  /** pk 必填 */
  private String pk;
  /** 必填 */
  private long startTime;
  /** 必填 */
  private long endTime;
}
