package me.hekr.iotos.api.dto.klink;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TopoSub {
  protected String pk;
  protected String devId;
  private String random;
  private String hashMethod;
  private String sign;

  public TopoSub() {}

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public String getRandom() {
    return this.random;
  }

  public String getHashMethod() {
    return this.hashMethod;
  }

  public String getSign() {
    return this.sign;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }

  public void setRandom(String random) {
    this.random = random;
  }

  public void setHashMethod(String hashMethod) {
    this.hashMethod = hashMethod;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  @Override
  public String toString() {
    return "TopoSub(super="
        + super.toString()
        + ", pk="
        + this.getPk()
        + ", devId="
        + this.getDevId()
        + ", random="
        + this.getRandom()
        + ", hashMethod="
        + this.getHashMethod()
        + ", sign="
        + this.getSign()
        + ")";
  }
}
