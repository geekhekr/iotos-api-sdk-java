package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import me.hekr.iotos.api.enums.CheckType;
import me.hekr.iotos.api.enums.DataType;

@JsonTypeInfo(use = Id.NAME, include = As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
  @Type(value = DataValueNoneChecker.class, name = "NONE"),
  @Type(value = DataValueEnumChecker.class, name = "ENUM"),
  @Type(value = DataValueRangeChecker.class, name = "RANGE"),
  @Type(value = DataValueLengthChecker.class, name = "LENGTH")
})
public interface DataValueChecker {
  boolean isValid(DataType var1, Object var2);

  CheckType getType();

  boolean canConvertTo(DataType var1);

  boolean valid(DataType var1);
}
