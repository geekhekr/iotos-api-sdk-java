package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class CloudSend extends KlinkDev {
  private ModelData data;

  public CloudSend() {}

  @Override
  public String getAction() {
    return Action.CLOUD_SEND.getAction();
  }

  public ModelData getData() {
    return this.data;
  }

  public void setData(ModelData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "CloudSend(super=" + super.toString() + ", data=" + this.getData() + ")";
  }
}
