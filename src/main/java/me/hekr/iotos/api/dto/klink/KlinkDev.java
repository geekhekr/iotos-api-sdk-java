package me.hekr.iotos.api.dto.klink;

public class KlinkDev extends Klink {
  private static final long serialVersionUID = 78156295896577172L;
  protected String pk;
  protected String devId;

  public KlinkDev() {}

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }

  @Override
  public String toString() {
    return "KlinkDev(super="
        + super.toString()
        + ", pk="
        + this.getPk()
        + ", devId="
        + this.getDevId()
        + ")";
  }
}
