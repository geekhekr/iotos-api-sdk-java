package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class AddTopo extends KlinkDev {
  private TopoSub sub;

  public AddTopo() {}

  @Override
  public String getAction() {
    return Action.ADD_TOPO.getAction();
  }

  public TopoSub getSub() {
    return this.sub;
  }

  public void setSub(TopoSub sub) {
    this.sub = sub;
  }

  @Override
  public String toString() {
    return "AddTopo(super=" + super.toString() + ", sub=" + this.getSub() + ")";
  }
}
