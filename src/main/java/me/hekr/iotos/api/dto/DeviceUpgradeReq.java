package me.hekr.iotos.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import me.hekr.iotos.api.enums.UpgradeType;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceUpgradeReq {

  @NotEmpty(message = "pk不能为空")
  private String pk;

  private UpgradeType upgradeType;
  private String desc;
  private String fromVersion;
  /** （要升级到的）目标版本，必填 */
  private String toVersion;

  private String md5;
  private String url;

  /** 是不是任务版本，如果true，则代表是从任意版本都可以升级到指定版本，fromVersion失效 */
  private boolean fromAnyVersion;

  /** 是否要升级所有设备 */
  private boolean upgradeAll;

  /**
   * 要升级的设备
   *
   * @see #upgradeAll 如果为true则忽略此项；否则 devIdList 不能为空
   */
  private List<String> devIdList;
}
