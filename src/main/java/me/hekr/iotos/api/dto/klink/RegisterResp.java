package me.hekr.iotos.api.dto.klink;

//
import me.hekr.iotos.api.enums.Action;
import me.hekr.iotos.api.enums.ErrorCode;

public class RegisterResp extends KlinkResp {
  private static final long serialVersionUID = -6728983939835762139L;
  private String devSecret;

  public RegisterResp(ErrorCode errorCode, String desc) {
    super(errorCode, desc);
  }

  @Override
  public String getAction() {
    return Action.REGISTER_RESP.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof RegisterResp)) {
      return false;
    } else {
      RegisterResp other = (RegisterResp) o;
      if (!other.canEqual(this)) {
        return false;
      } else if (!super.equals(o)) {
        return false;
      } else {
        Object this$devSecret = this.getDevSecret();
        Object other$devSecret = other.getDevSecret();
        if (this$devSecret == null) {
          if (other$devSecret != null) {
            return false;
          }
        } else if (!this$devSecret.equals(other$devSecret)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof RegisterResp;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    Object $devSecret = this.getDevSecret();
    result = result * 59 + ($devSecret == null ? 43 : $devSecret.hashCode());
    return result;
  }

  public String getDevSecret() {
    return this.devSecret;
  }

  public void setDevSecret(String devSecret) {
    this.devSecret = devSecret;
  }

  @Override
  public String toString() {
    return "RegisterResp(super=" + super.toString() + ", devSecret=" + this.getDevSecret() + ")";
  }

  public RegisterResp() {}
}
