package me.hekr.iotos.api.dto;

import lombok.Data;

@Data
public class ParamValueQuery {
  private String pk;
  private String devId;
  /** 必填 */
  private long startTime;
  /** 必填 */
  private long endTime;
  /** 参数 必填 */
  private String key;
}
