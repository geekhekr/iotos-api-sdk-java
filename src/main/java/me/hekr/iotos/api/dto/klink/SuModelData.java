package me.hekr.iotos.api.dto.klink;

public class SuModelData extends ModelData {
  private static final long serialVersionUID = -136551194596662462L;
  private String pk;
  private String devId;

  public SuModelData() {}

  @Override
  public String toString() {
    return "SuModelData(super="
        + super.toString()
        + ", pk="
        + this.getPk()
        + ", devId="
        + this.getDevId()
        + ")";
  }

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }
}
