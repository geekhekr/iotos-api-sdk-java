package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import me.hekr.iotos.api.enums.CheckType;
import me.hekr.iotos.api.enums.DataType;

@JsonTypeName("NONE")
public class DataValueNoneChecker implements DataValueChecker {

  @Override
  @JsonIgnore
  public boolean isValid(DataType dataType, Object value) {
    return DataType.isValid(dataType, value);
  }

  @Override
  @JsonIgnore
  public boolean canConvertTo(DataType dataType) {
    return true;
  }

  @Override
  public boolean valid(DataType dataType) {
    return true;
  }

  @Override
  public CheckType getType() {
    return CheckType.NONE;
  }
}
