package me.hekr.iotos.api.dto;

import me.hekr.iotos.api.dto.klink.Dev;
import me.hekr.iotos.api.dto.klink.KlinkResp;
import me.hekr.iotos.api.enums.Action;
import me.hekr.iotos.api.enums.ErrorCode;

public class DelTopoResp extends KlinkResp {
  private Dev sub;

  public DelTopoResp(ErrorCode errorCode, String desc) {
    super(errorCode, desc);
  }

  public DelTopoResp(ErrorCode errorCode) {
    super(errorCode);
  }

  @Override
  public String getAction() {
    return Action.DEL_TOPO_RESP.getAction();
  }

  public Dev getSub() {
    return this.sub;
  }

  public void setSub(Dev sub) {
    this.sub = sub;
  }

  @Override
  public String toString() {
    return "DelTopoResp(super=" + super.toString() + ", sub=" + this.getSub() + ")";
  }

  public DelTopoResp() {}
}
