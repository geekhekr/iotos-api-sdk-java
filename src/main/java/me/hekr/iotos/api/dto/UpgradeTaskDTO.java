package me.hekr.iotos.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import me.hekr.iotos.api.enums.UpgradeType;

/**
 * @author du
 */
@Data
public class UpgradeTaskDTO implements Serializable {

  protected String id;
  protected String pk;
  /** 要升级的版本，可以为空 ，空代表从无版本号的升级到目标版本 */
  protected String fromVersion;

  /** 是不是任务版本，如果true，则代表是从任意版本都可以升级到指定版本，fromVersion失效 */
  protected boolean fromAnyVersion;

  /** （要升级到的）目标版本，必填 */
  protected String toVersion;

  /** 是否要升级所有设备 */
  protected boolean upgradeAll;

  /**
   * 要升级的设备
   *
   * @see #upgradeAll 如果为true则忽略此项；否则 devIdList 不能为空
   */
  protected List<String> devIdList;

  protected Date createdDate;

  protected Date updatedDate;

  /** true 启用，设备符合升级条件就要进行处理；禁用false，即使符合也不做升级处理 */
  protected boolean enable;

  /** 删除标记 */
  protected boolean deleted;

  /** 文件md5 */
  protected String md5;
  /** 下载地址，http(s) */
  protected String url;
  /** 描述 */
  protected String desc;

  /** 内容，可以为空，建议填写，可以直接查看内容 */
  protected String content;

  protected UpgradeType type;

  /** 仅仅用于获取远程配置的devId，软硬件升级使用 devIdList */
  private String devId;
}
