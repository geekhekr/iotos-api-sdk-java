package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class ReportFirmware extends KlinkDev {
  private String type;
  private String version;

  public ReportFirmware() {}

  @Override
  public String getAction() {
    return Action.REPORT_FIRMWARE.getAction();
  }

  public String getType() {
    return this.type;
  }

  public String getVersion() {
    return this.version;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  @Override
  public String toString() {
    return "ReportFirmware(super="
        + super.toString()
        + ", type="
        + this.getType()
        + ", version="
        + this.getVersion()
        + ")";
  }
}
