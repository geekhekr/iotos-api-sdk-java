package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class GetTopo extends KlinkDev {
  private static final long serialVersionUID = 4118414118485680261L;

  public GetTopo() {}

  @Override
  public String getAction() {
    return Action.GET_TOPO.getAction();
  }

  @Override
  public String toString() {
    return "GetTopo(super=" + super.toString() + ")";
  }
}
