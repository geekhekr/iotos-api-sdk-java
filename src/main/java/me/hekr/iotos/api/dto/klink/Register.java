package me.hekr.iotos.api.dto.klink;

import java.util.List;
import me.hekr.iotos.api.dto.Tag;
import me.hekr.iotos.api.enums.Action;

public class Register extends KlinkDev {
  private static final long serialVersionUID = 6099206158766831129L;
  private String random;
  private String hashMethod;
  private String sign;
  private String name;
  private String imei;
  private String batchName;
  private List<Tag> tags;

  @Override
  public String getAction() {
    return Action.REGISTER.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof Register)) {
      return false;
    } else {
      Register other = (Register) o;
      if (!other.canEqual(this)) {
        return false;
      } else if (!super.equals(o)) {
        return false;
      } else {
        label97:
        {
          Object this$random = this.getRandom();
          Object other$random = other.getRandom();
          if (this$random == null) {
            if (other$random == null) {
              break label97;
            }
          } else if (this$random.equals(other$random)) {
            break label97;
          }

          return false;
        }

        Object this$hashMethod = this.getHashMethod();
        Object other$hashMethod = other.getHashMethod();
        if (this$hashMethod == null) {
          if (other$hashMethod != null) {
            return false;
          }
        } else if (!this$hashMethod.equals(other$hashMethod)) {
          return false;
        }

        Object this$sign = this.getSign();
        Object other$sign = other.getSign();
        if (this$sign == null) {
          if (other$sign != null) {
            return false;
          }
        } else if (!this$sign.equals(other$sign)) {
          return false;
        }

        label76:
        {
          Object this$name = this.getName();
          Object other$name = other.getName();
          if (this$name == null) {
            if (other$name == null) {
              break label76;
            }
          } else if (this$name.equals(other$name)) {
            break label76;
          }

          return false;
        }

        Object this$imei = this.getImei();
        Object other$imei = other.getImei();
        if (this$imei == null) {
          if (other$imei != null) {
            return false;
          }
        } else if (!this$imei.equals(other$imei)) {
          return false;
        }

        Object this$batchName = this.getBatchName();
        Object other$batchName = other.getBatchName();
        if (this$batchName == null) {
          if (other$batchName != null) {
            return false;
          }
        } else if (!this$batchName.equals(other$batchName)) {
          return false;
        }

        Object this$tags = this.getTags();
        Object other$tags = other.getTags();
        if (this$tags == null) {
          if (other$tags != null) {
            return false;
          }
        } else if (!this$tags.equals(other$tags)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof Register;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    Object $random = this.getRandom();
    result = result * 59 + ($random == null ? 43 : $random.hashCode());
    Object $hashMethod = this.getHashMethod();
    result = result * 59 + ($hashMethod == null ? 43 : $hashMethod.hashCode());
    Object $sign = this.getSign();
    result = result * 59 + ($sign == null ? 43 : $sign.hashCode());
    Object $name = this.getName();
    result = result * 59 + ($name == null ? 43 : $name.hashCode());
    Object $imei = this.getImei();
    result = result * 59 + ($imei == null ? 43 : $imei.hashCode());
    Object $batchName = this.getBatchName();
    result = result * 59 + ($batchName == null ? 43 : $batchName.hashCode());
    Object $tags = this.getTags();
    result = result * 59 + ($tags == null ? 43 : $tags.hashCode());
    return result;
  }

  public Register() {}

  public String getRandom() {
    return this.random;
  }

  public String getHashMethod() {
    return this.hashMethod;
  }

  public String getSign() {
    return this.sign;
  }

  public String getName() {
    return this.name;
  }

  public String getImei() {
    return this.imei;
  }

  public String getBatchName() {
    return this.batchName;
  }

  public List<Tag> getTags() {
    return this.tags;
  }

  public void setRandom(String random) {
    this.random = random;
  }

  public void setHashMethod(String hashMethod) {
    this.hashMethod = hashMethod;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }

  public void setBatchName(String batchName) {
    this.batchName = batchName;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  @Override
  public String toString() {
    return "Register(random="
        + this.getRandom()
        + ", hashMethod="
        + this.getHashMethod()
        + ", sign="
        + this.getSign()
        + ", name="
        + this.getName()
        + ", imei="
        + this.getImei()
        + ", batchName="
        + this.getBatchName()
        + ", tags="
        + this.getTags()
        + ")";
  }
}
