package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class DevUpgrade extends KlinkDev {
  private static final long serialVersionUID = 4913904608150992167L;
  private String url;
  private String md5;
  private String version;
  private String type;

  @Override
  public String getAction() {
    return Action.DEV_UPGRADE.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof DevUpgrade)) {
      return false;
    } else {
      DevUpgrade other = (DevUpgrade) o;
      if (!other.canEqual(this)) {
        return false;
      } else if (!super.equals(o)) {
        return false;
      } else {
        label61:
        {
          Object this$url = this.getUrl();
          Object other$url = other.getUrl();
          if (this$url == null) {
            if (other$url == null) {
              break label61;
            }
          } else if (this$url.equals(other$url)) {
            break label61;
          }

          return false;
        }

        label54:
        {
          Object this$md5 = this.getMd5();
          Object other$md5 = other.getMd5();
          if (this$md5 == null) {
            if (other$md5 == null) {
              break label54;
            }
          } else if (this$md5.equals(other$md5)) {
            break label54;
          }

          return false;
        }

        Object this$version = this.getVersion();
        Object other$version = other.getVersion();
        if (this$version == null) {
          if (other$version != null) {
            return false;
          }
        } else if (!this$version.equals(other$version)) {
          return false;
        }

        Object this$type = this.getType();
        Object other$type = other.getType();
        if (this$type == null) {
          if (other$type != null) {
            return false;
          }
        } else if (!this$type.equals(other$type)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof DevUpgrade;
  }

  @Override
  public int hashCode() {

    int result = super.hashCode();
    Object $url = this.getUrl();
    result = result * 59 + ($url == null ? 43 : $url.hashCode());
    Object $md5 = this.getMd5();
    result = result * 59 + ($md5 == null ? 43 : $md5.hashCode());
    Object $version = this.getVersion();
    result = result * 59 + ($version == null ? 43 : $version.hashCode());
    Object $type = this.getType();
    result = result * 59 + ($type == null ? 43 : $type.hashCode());
    return result;
  }

  public DevUpgrade() {}

  public String getUrl() {
    return this.url;
  }

  public String getMd5() {
    return this.md5;
  }

  public String getVersion() {
    return this.version;
  }

  public String getType() {
    return this.type;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setMd5(String md5) {
    this.md5 = md5;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "DevUpgrade(url="
        + this.getUrl()
        + ", md5="
        + this.getMd5()
        + ", version="
        + this.getVersion()
        + ", type="
        + this.getType()
        + ")";
  }
}
