package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class DevLogout extends KlinkDev {
  private String reason;

  @Override
  public String getAction() {
    return Action.DEV_LOGOUT.getAction();
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    } else if (!(o instanceof DevLogout)) {
      return false;
    } else {
      DevLogout other = (DevLogout) o;
      if (!other.canEqual(this)) {
        return false;
      } else if (!super.equals(o)) {
        return false;
      } else {
        Object this$reason = this.getReason();
        Object other$reason = other.getReason();
        if (this$reason == null) {
          if (other$reason != null) {
            return false;
          }
        } else if (!this$reason.equals(other$reason)) {
          return false;
        }

        return true;
      }
    }
  }

  protected boolean canEqual(Object other) {
    return other instanceof DevLogout;
  }

  public DevLogout() {}

  public String getReason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  @Override
  public String toString() {
    return "DevLogout(reason=" + this.getReason() + ")";
  }
}
