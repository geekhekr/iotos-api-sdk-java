package me.hekr.iotos.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import me.hekr.iotos.api.enums.DeviceType;

@Data
public class DeviceApiDTO implements Serializable {

  private static final long serialVersionUID = 6148192738636573612L;

  /** 顶级节点设备的pk，不存在则没有顶级节点，自身为顶级节点 */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  protected String rootPk;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  protected String name;
  /**
   * 顶级节点设备的pk，不存在则没有顶级节点，自身为顶级节点
   *
   * <p>parentPk 和 parentDevId 要么同时存在，要么同时不存在
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  protected String rootDevId;

  /** 父节点设备的pk，不存在则没有父节点 */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  protected String parentPk;
  /**
   * 父节点设备的devId，不存在则没有父节点;
   *
   * <p>parentPk 和 parentDevId 要么同时存在，要么同时不存在
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  protected String parentDevId;
  /** 创建时间 */
  protected Date createdDate;
  /** 更新时间 */
  protected Date updatedDate;

  /** 是否启用，false 禁用，否则启用 */
  protected Boolean enable;

  protected String pk;
  protected String devId;

  /** 在线状态，如果是子设备，这个状态在数据库中并不随着父节点的状态发生变更，只有查询返回的时候动态设置 */
  protected boolean online;

  /** 设备密钥 */
  protected String devSecret;

  /** 最近一次登录时间 */
  protected Date loginTime;

  /** 最后一次登出时间 */
  protected Date logoutTime;

  /** 注册时间 */
  protected Date registerTime;

  /** 第一次登录时间，也算是上电激活时间 */
  protected Date firstLoginTime;

  protected DeviceType deviceType;

  protected IpInfo ipInfo;

  /** 模组固件版本 */
  protected String moduleVersion;
  /** mcu固件版本 */
  protected String mcuVersion;
  /** 远程配置版本 */
  protected String configVersion;

  /** 批次添加名字 */
  protected String batchName;
  /** 设备 imei 如果有的话，全局唯一 */
  protected String imei;

  protected List<Tag> tags;

  /** 产品品牌 */
  private String brand;

  /** 产品型号 */
  private String model;

  /** 设备地址 */
  private String address;

  /**位置信息类别，1-楼宇 2-电梯 3-车库 4-室外*/
  private Integer orgCode;

  /** 所属镇街 */
  private String town;

  /** 所属村社 */
  private String village;

  /** 经纬度 */
  private Double[] location;

  /** z轴 */
  private String zaxis;


  /**
   * 设备是否被启用
   *
   * @return true 启用，否则禁用
   */
  public boolean isEnable() {
    return !Boolean.FALSE.equals(enable);
  }

  public Boolean getEnable() {
    return isEnable();
  }
}
