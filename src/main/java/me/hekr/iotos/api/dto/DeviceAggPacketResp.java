package me.hekr.iotos.api.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceAggPacketResp {
  /** 平均值列表 */
  private List<DeviceAggPacket> avgList;
  /** 最大值列表 */
  private List<DeviceAggPacket> maxList;
  /** 最小值列表 */
  private List<DeviceAggPacket> minList;
}
