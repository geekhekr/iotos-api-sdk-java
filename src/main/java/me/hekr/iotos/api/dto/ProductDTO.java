package me.hekr.iotos.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import lombok.Data;
import me.hekr.iotos.api.enums.DataCheckMode;
import me.hekr.iotos.api.enums.DataFormat;
import me.hekr.iotos.api.enums.DeviceLinkType;
import me.hekr.iotos.api.enums.DeviceType;
import me.hekr.iotos.api.enums.TransferType;

@Data
public class ProductDTO implements Serializable {
  private static final long serialVersionUID = 2147481103660105166L;
  protected String pk;
  protected boolean deleted;
  protected Date createdDate;
  protected Date updatedDate;
  protected DeviceType deviceType;
  protected String productSecret;
  protected List<Tag> tags;
  protected boolean dynamicRegister;
  protected boolean registerCheck;
  protected boolean loginCheck;
  protected boolean supportRemoteConfig;
  protected DataCheckMode dataCheckMode;
  private Integer quota;
  private Integer usedQuota;
  private DataFormat dataFormat;
  /** 联网方式 */
  private DeviceLinkType deviceLinkType;

  /** 数据传输类型 */
  private TransferType transferType;

  private String name;

  /** 型号 */
  private String model;
  /** 品牌 */
  private String brand;

  private String remark;
  private boolean open;

  /** 不可见产品将被隐藏 */
  private boolean hide;
  /** 来自哪个pk的拷贝 */
  private String copyFromPk;

  /** 数据来源部门 */
  private String dept;

  /** 省主题 */
  private String provinceTopic;

  /** 区主题 */
  private String districtTopic;

  /** 行业分类 */
  private String industry;

  /** 数据级别 */
  private String dataLevel;

  /** 共享属性 */
  private String shareProp;

  /** 开放属性 */
  private String openProp;

  /** 资源领域 */
  private String scope;

  /** 资源范围描述 */
  private String scopeDesc;

  /** 信息资源描述 */
  private String desc;

  /** 周期 */
  private String period;

  /** 更新时间 */
  private String updateTime;

  /** 业务系统编码 */
  private String businessCode;

  /** 所属业务系统 */
  private String businessSys;

  public DataFormat getDataFormat() {
    return this.dataFormat == null ? DataFormat.KLINK : this.dataFormat;
  }

  public DataCheckMode getDataCheckMode() {
    return this.dataCheckMode == null ? DataCheckMode.STRICT : this.dataCheckMode;
  }
}
