package me.hekr.iotos.api.dto.klink;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Map;
import me.hekr.iotos.api.enums.ErrorCode;

public class KlinkResp extends Klink {
  private static final long serialVersionUID = -4341021820638489039L;
  protected String pk;
  protected String devId;
  protected int code;

  @JsonInclude(Include.NON_NULL)
  protected String desc;

  @JsonInclude(Include.NON_NULL)
  protected Map<String, Object> params;

  public KlinkResp() {
    this.code = ErrorCode.SUCCESS.getCode();
  }

  public KlinkResp(ErrorCode errorCode) {
    this(errorCode, (String) null);
  }

  public KlinkResp(ErrorCode errorCode, String desc, Map<String, Object> params) {
    this.code = errorCode.getCode();
    this.desc = errorCode.getDesc();
    if (desc != null) {
      this.desc = this.desc + ", " + desc;
    }

    this.params = params;
  }

  public KlinkResp(ErrorCode errorCode, String desc) {
    this(errorCode, desc, (Map) null);
  }

  @JsonIgnore
  public boolean isSuccess() {
    return this.code == ErrorCode.SUCCESS.getCode();
  }

  public KlinkResp setErrorCode(ErrorCode errorCode) {
    this.code = errorCode.getCode();
    this.desc = errorCode.getDesc();
    return this;
  }

  @Override
  public String toString() {
    return "KlinkResp(super="
        + super.toString()
        + ", pk="
        + this.getPk()
        + ", devId="
        + this.getDevId()
        + ", code="
        + this.getCode()
        + ", desc="
        + this.getDesc()
        + ", params="
        + this.getParams()
        + ")";
  }

  public String getPk() {
    return this.pk;
  }

  public String getDevId() {
    return this.devId;
  }

  public int getCode() {
    return this.code;
  }

  public String getDesc() {
    return this.desc;
  }

  public Map<String, Object> getParams() {
    return this.params;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public void setDevId(String devId) {
    this.devId = devId;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public void setParams(Map<String, Object> params) {
    this.params = params;
  }
}
