package me.hekr.iotos.api.dto.klink;

import me.hekr.iotos.api.enums.Action;

public class GetConfig extends KlinkDev {
  private static final long serialVersionUID = -8668881267356068518L;

  public GetConfig() {}

  @Override
  public String getAction() {
    return Action.GET_CONFIG.getAction();
  }

  @Override
  public String toString() {
    return "GetConfig(super=" + super.toString() + ")";
  }
}
