package me.hekr.iotos.api.exception;

import lombok.Getter;
import lombok.Setter;
import me.hekr.iotos.api.IotErrorResponse;

@Getter
@Setter
public class IotException extends RuntimeException {

  private int code = 400;
  private IotErrorResponse innerErrorResp;

  public IotException() {
    super();
  }

  public IotException(int code) {
    this.code = code;
  }

  public IotException(int code, String message, Throwable cause) {
    super(message, cause);
    this.code = code;
  }

  public IotException(int code, String message) {
    super(message);
    this.code = code;
  }
}
