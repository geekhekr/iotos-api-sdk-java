package me.hekr.iotos.api;

import lombok.Data;

@Data
public class IotErrorResponse {
  private int code;
  private String msg;
}
