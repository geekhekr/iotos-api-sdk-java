package me.hekr.iotos.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.apache.commons.codec.binary.Base64;

public class JsonUtil {
  private static ObjectMapper objectMapper = new ObjectMapper();

  public JsonUtil() {}

  public static JsonNode parse(String str) throws IOException {
    try {
      return (JsonNode) objectMapper.readValue(str, JsonNode.class);
    } catch (Throwable var2) {
      throw var2;
    }
  }

  public static String toJson(Object obj) throws JsonProcessingException {
    try {
      return objectMapper.writeValueAsString(obj);
    } catch (Throwable var2) {
      throw var2;
    }
  }

  public static byte[] toBytes(Object obj) throws JsonProcessingException {
    try {
      return objectMapper.writeValueAsBytes(obj);
    } catch (Throwable var2) {
      throw var2;
    }
  }

  public static String toBase64(Object obj) throws JsonProcessingException {
    try {
      return Base64.encodeBase64String(objectMapper.writeValueAsBytes(obj));
    } catch (Throwable var2) {
      throw var2;
    }
  }

  public static <T> T fromJson(String payload, Class<T> tClass) throws IOException {
    try {
      return objectMapper.readValue(payload, tClass);
    } catch (Throwable var3) {
      throw var3;
    }
  }

  public static <T> T fromJson(String payload, TypeReference<T> tTypeReference) throws IOException {
    try {
      return objectMapper.readValue(payload, tTypeReference);
    } catch (Throwable var3) {
      throw var3;
    }
  }

  public static <T> T fromBase64(String payload, Class<T> tClass) throws IOException {
    try {
      return objectMapper.readValue(Base64.decodeBase64(payload), tClass);
    } catch (Throwable var3) {
      throw var3;
    }
  }

  public static <T> T fromBytes(byte[] payload, Class<T> tClass) throws IOException {
    try {
      return objectMapper.readValue(payload, tClass);
    } catch (Throwable var3) {
      throw var3;
    }
  }

  public static <T> T fromBase64(String payload, TypeReference<T> tTypeReference)
      throws IOException {
    try {
      return objectMapper.readValue(Base64.decodeBase64(payload), tTypeReference);
    } catch (Throwable var3) {
      throw var3;
    }
  }

  public static <T> T convert(Object obj, Class<T> clazz) {
    return objectMapper.convertValue(obj, clazz);
  }

  public static <T> T convert(Object obj, TypeReference<T> tTypeReference) {
    return objectMapper.convertValue(obj, tTypeReference);
  }

  static {
    objectMapper
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        .configure(DeserializationFeature.FAIL_ON_TRAILING_TOKENS, true);
  }
}
