package me.hekr.iotos.api.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.Getter;

public enum UpgradeType {
  /** 模组 */
  MODULE("module"),
  /** mcu */
  MCU("mcu"),

  /** 远程配置 */
  CONFIG("config");

  public static final Map<String, UpgradeType> MAP;

  static {
    MAP =
        Arrays.stream(UpgradeType.values())
            .collect(Collectors.toMap(t -> t.type, Function.identity()));
  }

  @Getter private final String type;

  UpgradeType(String type) {
    this.type = type;
  }

  public static UpgradeType of(String type) {
    return MAP.get(type);
  }
}
