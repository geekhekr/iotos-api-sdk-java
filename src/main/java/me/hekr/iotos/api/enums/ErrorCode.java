package me.hekr.iotos.api.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum ErrorCode {
  SUCCESS(0, "success"),
  UNDEFINED(65535, "undefined"),
  PARAM_INVALID(4000, "param invalid"),
  JSON_PARSE_ERROR(1001, "json parse error"),
  ACTION_NOT_SUPPORT(1002, "action not support"),
  DEVICE_NOT_LOGIN(1003, "device not login"),
  DEVICE_NOT_EXIST(1004, "device not exist"),
  DEVICE_LOGIN_AUTH_FAILED(1005, "device login auth failed"),
  DEVICE_NOT_SUPPORT_SUB_DEV(1006, "device not support sub device"),
  DEVICE_TOPO_NOT_ALLOW_CIRCLE(1007, "device topo not allow circle"),
  DEVICE_NOT_ONLINE(1008, "device not online"),
  DEVICE_DISABLED(1009, "device is disabled"),
  DEVICE_NOT_HAS_SUB_DEVICE(1010, "device not has the sub device"),
  PRODUCT_NOT_EXIST(1104, "product not exist"),
  PRODUCT_NOT_SUPPORT_REMOTE_CONFIG(1105, "product not support remote config"),
  PRODUCT_DELETED(1106, "product deleted"),
  DEVICE_IMEI_INVALID(1107, "device imei invalid"),
  DEVICE_IMEI_EXIST(1108, "device imei exist"),
  RULE_SELECT_PARAM_INVALID(1109, "rule select param not valid"),
  RULE_EXPRESSION_INVALID(1110, "rule expression not valid"),
  RULE_NOT_FOUND(1111, "rule not found"),
  RULE_DEST_HTTP_NOT_VALID(1112, "rule dest http url not valid"),
  MODEL_DATA_PARAM_INVALID(1113, "model params not valid"),
  QUOTA_EXIST(1114, "quota exist"),
  QUOTA_NOT_FOUND(1115, "quota not found"),
  UPGRADE_TASK_ENABLE_CONFLICT(1116, "upgrade task enable conflict"),
  UPGRADE_TASK_ENABLE_CYCLE(1117, "upgrade task has cycle"),
  UPGRADE_TASK_NOT_FOUND(1118, "upgrade task not found"),
  AVAILABLE_REMOTE_CONFIG_NOT_FOUND(1119, "available remote config not found"),
  QUOTA_INSUFFICIENT(1120, "quota insufficient"),
  MODEL_DATA_PARAM_NAME_CONFLICT(1121, "model data param name conflict"),
  MODEL_DATA_PARAM_IDENTIFIER_CONFLICT(1122, "model data param identifier conflict"),
  MODEL_DATA_CMD_NAME_CONFLICT(1123, "model data cmd name conflict"),
  MODEL_DATA_CMD_IDENTIFIER_CONFLICT(1124, "model data cmd identifier conflict"),
  DEV_ID_INVALID(1125, "devId invalid"),
  DEV_ID_EXIST(1126, "device id exist"),
  PRODUCT_NOT_ALLOW_DELETE_DEVICE_EXIST(1127, "product has devices not allow to delete"),
  DEVICE_NOT_ALLOW_TO_DIRECT_CONNECT(1128, "device(sub) not allow to direct connect"),
  DEVICE_GATEWAY_NOT_ONLINE(1129, "device's gateway not online"),
  PRODUCT_SCRIPT_NOT_RUNNING(1130, "product script not running or not exist"),
  PRODUCT_SCRIPT_ENCODE_ERROR(1131, "product script encode error"),
  PRODUCT_SCRIPT_DECODE_ERROR(1132, "product script decode error"),
  PRODUCT_NOT_SUPPORT_SCRIPT(1133, "product not support script"),
  PRODUCT_SCRIPT_RETURN_VALUE_INVALID(1134, "product script return value invalid"),
  PRODUCT_SCRIPT_COMPILE_ERROR(1135, "product script compile error"),
  DEVICE_NOT_SUPPORT_BATCH_DEV_SEND(
      1136, "device not support batch dev send, only gateway support"),
  DEVICE_TOPO_NOT_EXIST(1137, "device topo not exist"),
  PAYLOAD_IS_EMPTY(1138, "payload is empty"),
  DEVICE_LOGIN_TOO_FREQUENTLY(1139, "device login too frequently"),
  TAG_KEY_INVALID(1140, "tag key invalid, not allow null or start or end of blank"),
  TAG_KEY_DUPLICATED(1141, "tag key duplicated"),
  REMOTE_CONFIG_MULTI_DEVICE(1142, "remote config devices count max is one"),
  REMOTE_CONFIG_NOT_EXIST(1143, "remote config not exist"),
  PRODUCT_EXIST(1144, "product exist"),
  FILE_ERROR(1145, "file error"),
  INTERNAL_ERROR(500, "internal error");

  private static final Map<Integer, ErrorCode> ERROR_CODE_MAP =
      (Map)
          Arrays.stream(values())
              .collect(Collectors.toMap(ErrorCode::getCode, Function.identity()));
  private int code;
  private String desc;

  private ErrorCode(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  public static ErrorCode of(int code) {
    return (ErrorCode) ERROR_CODE_MAP.getOrDefault(code, UNDEFINED);
  }

  public int getCode() {
    return this.code;
  }

  public String getDesc() {
    return this.desc;
  }
}
