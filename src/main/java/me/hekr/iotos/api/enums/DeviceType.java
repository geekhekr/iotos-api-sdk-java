package me.hekr.iotos.api.enums;

public enum DeviceType {
  GENERAL,
  SWITCH,
  GATEWAY,
  TERMINAL;

  private DeviceType() {}

  public static boolean isDirectConnectType(DeviceType deviceType) {
    return deviceType == GENERAL || deviceType == GATEWAY;
  }
}
