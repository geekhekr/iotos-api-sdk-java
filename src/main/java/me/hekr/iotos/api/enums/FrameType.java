package me.hekr.iotos.api.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum FrameType {
  DEV_UP("up"),
  DEV_DOWN("down"),
  DEV_UP_DOWN("up_down"),
  INNER("inner");

  private static final Map<String, FrameType> TYPE_MAP =
      (Map)
          Arrays.stream(values())
              .collect(Collectors.toMap(FrameType::getType, Function.identity()));
  private String type;

  private FrameType(String type) {
    this.type = type;
  }

  public static FrameType of(String action) {
    return (FrameType) TYPE_MAP.get(action);
  }

  public String getType() {
    return this.type;
  }
}
