package me.hekr.iotos.api.enums;

import org.apache.commons.lang3.StringUtils;

/** Created by @author yan.li on 2020-01-20 NBOperator */
public enum NBOperator {
  /** 中国电信* */
  CHINA_TELECOM,
  /** 中国移动* */
  CHINA_MOBILE,
  /** 联通 */
  CHINA_UNICOM,
  /** 未知 */
  UNKNOWN;

  /**
   * 字符到 NBOperator枚举的转化
   *
   * @param operator operator
   * @return NBOperator
   */
  public static NBOperator of(String operator) {

    if (operator == null) {
      return UNKNOWN;
    }

    for (NBOperator nbOperator : NBOperator.values()) {
      if (StringUtils.equalsIgnoreCase(nbOperator.name(), operator)) {
        return nbOperator;
      }
    }

    return UNKNOWN;
  }
}
