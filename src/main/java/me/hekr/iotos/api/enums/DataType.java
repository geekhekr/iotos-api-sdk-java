package me.hekr.iotos.api.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import me.hekr.iotos.api.dto.DataValueChecker;
import me.hekr.iotos.api.dto.DataValueEnumChecker;
import me.hekr.iotos.api.dto.DataValueLengthChecker;
import me.hekr.iotos.api.dto.DataValueNoneChecker;
import me.hekr.iotos.api.dto.DataValueRangeChecker;

public enum DataType {
  STRING(
      new Class[] {
        DataValueNoneChecker.class, DataValueLengthChecker.class, DataValueEnumChecker.class
      }) {
    @Override
    public String convert(Object obj) {
      return String.valueOf(obj);
    }
  },
  INT(
      new Class[] {
        DataValueNoneChecker.class, DataValueRangeChecker.class, DataValueEnumChecker.class
      }) {
    @Override
    public Long convert(Object obj) {
      if (obj instanceof Number) {
        return ((Number) obj).longValue();
      } else {
        throw new NumberFormatException(obj + " is not number");
      }
    }
  },
  FLOAT(
      new Class[] {
        DataValueNoneChecker.class, DataValueRangeChecker.class, DataValueEnumChecker.class
      }) {
    @Override
    public Double convert(Object obj) {
      if (obj instanceof Number) {
        return ((Number) obj).doubleValue();
      } else {
        throw new NumberFormatException(obj + " is not number");
      }
    }
  };

  private Set<Class<? extends DataValueChecker>> supportCheckTypes;

  private DataType(Class<? extends DataValueChecker>... checkTypes) {
    if (checkTypes != null && checkTypes.length != 0) {
      this.supportCheckTypes = (Set) Arrays.stream(checkTypes).collect(Collectors.toSet());
    } else {
      this.supportCheckTypes = Collections.emptySet();
    }
  }

  public static DataType fromValue(Object value) {
    if (!(value instanceof Double) && !(value instanceof Float)) {
      if (value instanceof Number) {
        return INT;
      } else {
        return value instanceof String ? STRING : null;
      }
    } else {
      return FLOAT;
    }
  }

  public static boolean isValid(DataType dataType, Object value) {
    if (dataType == null) {
      return false;
    } else {
      DataType type = fromValue(value);
      if (type == null) {
        return false;
      } else {
        return dataType == type || type == INT && dataType == FLOAT;
      }
    }
  }

  public abstract <T> T convert(Object var1);

  public boolean isEq(Object v1, Object v2) {
    try {
      return this.convert(v1).equals(this.convert(v2));
    } catch (Exception var4) {
      return false;
    }
  }

  public boolean isSupport(DataValueChecker checkType) {
    return checkType == null ? false : this.supportCheckTypes.contains(checkType.getClass());
  }

  public String getSupportCheckTypesDesc() throws IllegalAccessException, InstantiationException {
    try {
      if (this.supportCheckTypes == null) {
        return "";
      } else {
        StringBuilder desc = new StringBuilder();
        Iterator var2 = this.supportCheckTypes.iterator();

        while (var2.hasNext()) {
          Class<? extends DataValueChecker> clazz = (Class) var2.next();
          desc.append(((DataValueChecker) clazz.newInstance()).getType().getType()).append(",");
        }

        return desc.length() == 0
            ? desc.toString()
            : desc.deleteCharAt(desc.length() - 1).toString();
      }
    } catch (Throwable var4) {
      throw var4;
    }
  }
}
