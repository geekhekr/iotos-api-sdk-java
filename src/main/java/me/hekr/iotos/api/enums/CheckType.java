package me.hekr.iotos.api.enums;

public enum CheckType {
  NONE("none"),
  RANGE("range"),
  ENUM("enum"),
  LENGTH("length");

  private String type;

  private CheckType(String type) {
    this.type = type;
  }

  public String getType() {
    return this.type;
  }
}
