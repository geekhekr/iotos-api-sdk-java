package me.hekr.iotos.api.enums;

public enum DataCheckMode {
  STRICT,
  LOOSE,
  LESS,
  MORE;

  private DataCheckMode() {}
}
