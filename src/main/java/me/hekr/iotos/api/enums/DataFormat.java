package me.hekr.iotos.api.enums;

public enum DataFormat {
  KLINK("KLink(标准格式)"),
  CUSTOM("自定义");

  private final String desc;

  private DataFormat(String desc) {
    this.desc = desc;
  }

  public String getDesc() {
    return this.desc;
  }
}
