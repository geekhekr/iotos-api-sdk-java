package me.hekr.iotos.api.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

/**
 * 设备联网方式
 *
 * @author du
 */
public enum DeviceLinkType {
  /** wifi */
  WIFI(
      "Wi-Fi",
      TransferType.MQTT,
      TransferType.HTTP,
      TransferType.TCP,
      TransferType.UDP,
      TransferType.OTHER),
  /** 蜂窝数据 2,3,4,5G */
  CELLUAR(
      "2/3/4/5G",
      TransferType.MQTT,
      TransferType.HTTP,
      TransferType.TCP,
      TransferType.UDP,
      TransferType.OTHER),
  NBIOT("NB-IoT", TransferType.COAP, TransferType.LWM2M, TransferType.NB_OPERATOR),
  /** 以太网(有线) */
  ETH(
      "以太网(有线) ",
      TransferType.MQTT,
      TransferType.HTTP,
      TransferType.TCP,
      TransferType.UDP,
      TransferType.OTHER),
  /** 其他 */
  OTHER(
      "其他",
      TransferType.MQTT,
      TransferType.HTTP,
      TransferType.TCP,
      TransferType.UDP,
      TransferType.OTHER),
  /** LoRa */
  LORA("LoRa", TransferType.LORA);

  @Getter private final String desc;
  @Getter private final List<TransferType> transferTypes;

  DeviceLinkType(String desc, TransferType transferType, TransferType... transferTypes) {
    this.desc = desc;
    List<TransferType> transferTypeList = new ArrayList<>();
    transferTypeList.add(transferType);
    if (transferTypes != null) {
      transferTypeList.addAll(Arrays.asList(transferTypes));
    }

    this.transferTypes = transferTypeList;
  }

  public boolean hasTransferType(TransferType transferType) {
    if (transferType == null) {
      return false;
    }

    return transferTypes.contains(transferType);
  }
}
