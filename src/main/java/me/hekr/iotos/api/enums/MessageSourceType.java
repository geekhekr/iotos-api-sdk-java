package me.hekr.iotos.api.enums;

public enum MessageSourceType {
  MQTT,
  REDIS_STEAM;

  private MessageSourceType() {}
}
