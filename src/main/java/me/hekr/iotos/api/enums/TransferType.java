package me.hekr.iotos.api.enums;

import static me.hekr.iotos.api.enums.DataFormat.CUSTOM;
import static me.hekr.iotos.api.enums.DataFormat.KLINK;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

/**
 * @author du
 *     <p>数据传输类型
 */
public enum TransferType {
  /** mqtt */
  MQTT("mqtt", KLINK, CUSTOM),
  /** http */
  HTTP("http", KLINK),
  /** tcp */
  TCP("tcp", KLINK, CUSTOM),
  /** udp */
  UDP("udp", KLINK, CUSTOM),
  /** CoAP nb-iot中的一种 */
  COAP("CoAP", KLINK, CUSTOM),
  /** lwm2m nb-iot 中的一种 */
  LWM2M("LwM2M", KLINK, CUSTOM),
  /** 运营商转发 */
  NB_OPERATOR("nb_operator", KLINK, CUSTOM),
  OTHER("其他", KLINK, CUSTOM),
  /** LoRa */
  LORA("LoRa", CUSTOM);
  @Getter private final String desc;
  @Getter private final List<DataFormat> dataFormats;

  TransferType(String desc, DataFormat dataFormat, DataFormat... dataFormats) {
    this.desc = desc;
    List<DataFormat> transferTypeList = new ArrayList<>();
    transferTypeList.add(dataFormat);
    if (dataFormats != null) {
      transferTypeList.addAll(Arrays.asList(dataFormats));
    }

    this.dataFormats = transferTypeList;
  }

  public boolean hasDataFormat(DataFormat dataFormat) {
    if (dataFormat == null) {
      return false;
    }

    return dataFormats.contains(dataFormat);
  }
}
