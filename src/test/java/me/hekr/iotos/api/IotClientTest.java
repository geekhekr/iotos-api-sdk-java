package me.hekr.iotos.api;

import java.util.*;

import me.hekr.iotos.api.dto.*;
import me.hekr.iotos.api.dto.klink.ModelData;
import me.hekr.iotos.api.dto.klink.TopoSub;
import me.hekr.iotos.api.enums.*;
import me.hekr.iotos.api.util.Pagec;
import org.junit.BeforeClass;
import org.junit.Test;

// @Ignore

public class IotClientTest {
  private static IotClient client;

  @BeforeClass
  public static void before() {

    client =
        IotClient.builder()
            .host("http://202.101.162.69:5679/console-api/")
            .ak("MO1jqqQrKZq4EXssDiBOE3hG")
            .as("6dYCX0xsn6eQZaf3dozV9gJjubGByO")
            .build();
  }

  @Test
  public void test() {
    System.out.println("233");
  }

  @Test
  public void testGetAllProducts() {
    System.out.println(client.getAllProducts());
  }

  @Test
  public void testGetLoginToken() {
    System.out.println(client.getLoginToken());
  }

  @Test
  public void getLoginToken() {
    System.out.println(client.getLoginToken());
  }

  @Test
  public void getProduct() {
    int page = 0;
    int size = 10;
    System.out.println(client.getProduct(page, size));
  }

  @Test
  public void adminGetSnapshot() {
    System.out.println(
        client.adminGetDeviceSnapshot("409127b74d2c4b3b9b86a5e6c08b72cc", "DTUMB20210423001101"));
  }

  @Test
  public void adminDelSub() {
    DeviceDTO deviceDTO = new DeviceDTO();
    deviceDTO.setPk("982cbe11ae2848a7b4ae493739240300");
    deviceDTO.setDevId("sub002");
    System.out.println(
        client.adminDelTopo("0a584188ac614cb089eec5778d0f5308", "gateway002", deviceDTO));
  }

  @Test
  public void adminAddSub() {
    DeviceDTO deviceDTO = new DeviceDTO();
    deviceDTO.setPk("982cbe11ae2848a7b4ae493739240300");
    deviceDTO.setDevId("sub002");
    System.out.println(
        client.adminAddTopo("0a584188ac614cb089eec5778d0f5308", "gateway002", deviceDTO));
  }

  @Test
  public void adminGetDevicePackets() {
    System.out.println(
        client.adminGetDevicePackage(
            "12ff59f46d2d4e2796443c9b919e6a32",
            "330109010002",
            1660492800000L,
            1660665599999L,
            "",
            0,
            10));
  }

  @Test
  public void adminDeviceInfo() {
    String pk = "6faa0966c5564dbabdd55d350df7c076";
    String devId = "2001090604";
    DeviceApiDTO deviceApiDTO = client.getAdminDevice(pk, devId);
    System.out.println(deviceApiDTO.toString());
  }

  @Test
  public void adminAddDevice() {
    DeviceAddReq reqDto = new DeviceAddReq();
    reqDto.setDevId("ttttttt");
    reqDto.setName("ceshi");
    reqDto.setAddress("ceshi");
    reqDto.setTown("ceshi");
    reqDto.setLocation(new Double[]{123.2,123.3});
    reqDto.setZaxis("12");
    reqDto.setVillage("ceshi");
    reqDto.setPk("6faa0966c5564dbabdd55d350df7c076");
    System.out.println(client.adminAddDevice(reqDto));
  }

  @Test
  public void getAdminProduct() {
    int page = 0;
    int size = 100;
    Pagec<ProductDTO> adminProduct = client.getAdminProduct(page, size);
    System.out.println(adminProduct);
  }

  @Test
  public void getBatchDevices() {
    String pk = "c33d447799e343f4bb2237390b82604d";
    String batchName = "20210908092913";
    System.out.println(client.getBatchDevices(pk, batchName));
  }

  @Test
  public void updateName() {
    DeviceUpdateNameReq req = new DeviceUpdateNameReq();
    req.setPk("c33d447799e343f4bb2237390b82604d");
    req.setDevId("device001");
    req.setName("dev00001");

    client.updateName(req);

    System.out.println("success!!");
  }

  @Test
  public void getDeviceStatus() {
    DevIdListReq req = new DevIdListReq();
    List<String> list = Arrays.asList("device001", "device002");
    req.setPk("c33d447799e343f4bb2237390b82604d");
    req.setDevIds(list);
    System.out.println(client.getDeviceStatus(req));
    System.out.println("success!!!");
  }

  @Test
  public void addDevice() {
    DeviceAddReq reqDto = new DeviceAddReq();
    reqDto.setDevId("device003");
    reqDto.setName("dev003");
    reqDto.setPk("c33d447799e343f4bb2237390b82604d");
    System.out.println(client.addDevice(reqDto));
  }

  @Test
  public void batchAddDevices() {

    DevIdListReq devIdListReq = new DevIdListReq();
    List<String> list = Arrays.asList("device001", "device002", "device003");

    devIdListReq.setPk("c33d447799e343f4bb2237390b82604d");
    devIdListReq.setDevIds(list);

    System.out.println(client.batchAddDevices(devIdListReq));
  }

  @Test
  public void creatProduct() {
    ConsoleProductReq consoleProductReq = new ConsoleProductReq();
    consoleProductReq.setName("测试对接设备");
    consoleProductReq.setCid("619352cdf871e0001534d638");
    consoleProductReq.setDataFormat(DataFormat.KLINK);
    consoleProductReq.setTransferType(TransferType.HTTP);
    consoleProductReq.setDeviceType(DeviceType.GENERAL);
    consoleProductReq.setDeviceLinkType(DeviceLinkType.OTHER);
    consoleProductReq.setDataCheckMode(DataCheckMode.LOOSE);
    consoleProductReq.setHide(true);
    consoleProductReq.setDept("414051780");
    ProductDTO product = client.createProduct(consoleProductReq);
    System.out.println(product);
  }

  @Test
  public void creatPotocol() {
    ModelProtocolDTO modelProtocolDTO = new ModelProtocolDTO();
    List<ModelParamDTO> param = new ArrayList<>();
    ModelParamDTO modelParamDTO = new ModelParamDTO();
    modelParamDTO.setParam("test");
    modelParamDTO.setUnit("v");
    modelParamDTO.setName("ceshi");

    modelParamDTO.setCheckType(new DataValueNoneChecker());
    modelParamDTO.setDataType(DataType.STRING);
    modelParamDTO.setFrameType(FrameType.DEV_UP);
    param.add(modelParamDTO);
    modelProtocolDTO.setParams(param);
    ModelProtocolDTO res =
        client.adminCreateModel("ec3bfc5273bc46d9a675453fd08f8cd8", modelProtocolDTO);
    System.out.println(res);
  }

  @Test
  public void editProduct() {
    ConsoleUpdateProductReq consoleProductReq = new ConsoleUpdateProductReq();
    consoleProductReq.setName("测试对接设备");
    consoleProductReq.setCid("619352cdf871e0001534d638");
    consoleProductReq.setPk("3256d8be6d4640fc8fd46fae6c10b4d9");
    consoleProductReq.setHide(true);
    consoleProductReq.setDept("414051780");
    ProductDTO product = client.editProduct(consoleProductReq);
    System.out.println(product);
  }

  @Test
  public void getDeviceList() {
    String pk = "646bce17f2834b0b83a21948c747b91e";
    String keyWord = "gate";
    int page = 0;
    DeviceType deviceType = DeviceType.GATEWAY;
    Boolean online = true;
    int size = 10;
    System.out.println(client.getDeviceList(pk, keyWord, page, deviceType, online, size));
  }

  @Test
  public void getAdminDeviceList() {
    String pk = "7d78997e466a497d92c4966d79ba5644";
    String keyWord = null;
    int page = 0;
    Boolean online = null;
    int size = 10;
    System.out.println(client.getAdminDeviceList(pk, keyWord, page, online, size));
  }

  @Test
  public void getAdminProductInfoListByPks() {
    Set<String> set = new HashSet<>();
    set.add("eed55fcd2f974b94b5806142125bd825");
    set.add("a86771fc156949b299d216358a8de631");
    List<ProductDTO> list = client.getAdminProductInfoListByPks(set);
    System.out.println(list);
  }

  @Test
  public void getModelByPks() {
    Set<String> set = new HashSet<>();
    set.add("47f6381c245b4ddb9ec0b5da0ba783e2");
    set.add("6390e0e3d59143f8a0c339013ca037ca");
    System.out.println(client.getAdminModelByPks(set));
  }

  @Test
  public void deviceInfo() {
    String pk = "c33d447799e343f4bb2237390b82604d";
    String devId = "device002";
    DeviceApiDTO deviceApiDTO = client.deviceInfo(pk, devId);
    System.out.println(deviceApiDTO.getName());
  }

  @Test
  public void getDeviceSnapshot() {
    String pk = "c33d447799e343f4bb2237390b82604d";
    String devId = "device001";
    System.out.println(client.getDeviceSnapshot(pk, devId));
  }

  @Test
  public void delDevice() {
    String pk = "c33d447799e343f4bb2237390b82604d";
    String devId = "device004";
    boolean delSnapshot = true;

    client.delDevice(pk, devId, delSnapshot);
  }

  @Test
  public void getDeviceHistoryData() {
    String pk = "c33d447799e343f4bb2237390b82604d";
    String devId = "device001";
    Long startTime = 10L;
    Long endTime = 100L;
    String action = "";
    int page = 0;
    int size = 10;
    System.out.println(
        client.getDeviceHistoryData(pk, devId, startTime, endTime, action, null, page, size));
  }

  @Test
  public void deviceCloudSend() {
    String pk = "646bce17f2834b0b83a21948c747b91e";
    String devId = "gate001";
    ModelData data = new ModelData();

    data.setCmd("setSwitch");
    Map<String, Object> map = new HashMap<>();
    map.put("switch", 0);

    data.setParams(map);
    System.out.println(client.deviceCloudSend(pk, devId, data));
  }

  @Test
  public void adminDeviceCloudSend() {
    String pk = "a34ce5b3111d4849b6cf7b54c4240bf5";
    String devId = "mqtt";
    ModelData data = new ModelData();

    data.setCmd("set");
    Map<String, Object> map = new HashMap<>();
    map.put("value", 0);

    data.setParams(map);
    System.out.println(client.adminDeviceCloudSend(pk, devId, data));
  }

  @Test
  public void cloudSendMsgInfo() {
    String messageId = "1";
    System.out.println(client.cloudSendMsgInfo(messageId));
  }

  @Test
  public void cloudSendMsgList() {
    int page = 0;
    int size = 10;
    String pk = "646bce17f2834b0b83a21948c747b91e";
    String devId = "gate001";
    Long startTime = null;
    Long endTime = null;
    System.out.println(client.cloudSendMsgList(page, size, pk, devId, startTime, endTime));
  }

  @Test
  public void addTopo1() {
    String pk = "646bce17f2834b0b83a21948c747b91e";
    String devId = "gate001";
    TopoSub sub = new TopoSub();
    sub.setDevId("device001");
    sub.setPk("c33d447799e343f4bb2237390b82604d");
    System.out.println(client.addTopo(pk, devId, sub));
  }

  @Test
  public void delTopo() {
    String pk = "646bce17f2834b0b83a21948c747b91e";
    String devId = "gate001";
    TopoSub sub = new TopoSub();
    sub.setDevId("device001");
    sub.setPk("c33d447799e343f4bb2237390b82604d");
    System.out.println(client.delTopo(pk, devId, sub));
  }

  @Test
  public void getSystem() {
    System.out.println(client.getSystem());
  }

  @Test
  public void getCatProduct() {
    System.out.println(client.getCatProduct());
  }
}
