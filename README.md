# 说明

本项目是 [氦氪](https://hekr.me/) [IoTOS](https://hekr.me/p-iotos.html) 的配套**专用**API开发 Java SDK，用来进行二次开发对接 IoTOS 平台的一套工具集合。
需要配合 IoTOS 平台使用。

本 SDK 托管在 [gitee](https://gitee.com/geekhekr/iotos-api-sdk-java) 以开源的方式提供给客户使用，使用 [MulanPSL-2.0](https://gitee.com/geekhekr/iotos-api-sdk-java/blob/master/LICENSE) 方式授权。

[IoTOS 文档地址](http://hy.hekr.me/iot-docs-test/web/index.html)

[API文档部分](http://hy.hekr.me/iot-docs-test/web/content/%E5%BA%94%E7%94%A8%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97/%E5%BA%94%E7%94%A8%E6%8E%A5%E5%85%A5.html)

## 使用说明

添加仓库

```xml
<repositories>
    <repository>
      <id>s01-sonatype</id>
      <url>https://s01.oss.sonatype.org/content/repositories/snapshots/</url>
    </repository>
</repositories>
```

引入依赖

```xml
<dependency>
  <groupId>me.hekr.iotos</groupId>
  <artifactId>sx-iotos-api-sdk</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
```

使用示例参考 `测试用例 IotClientTest`

