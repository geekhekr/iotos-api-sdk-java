#!/bin/bash
VERSION="1.0.11"
echo "版本号：$VERSION"
./mvnw clean
./mvnw versions:set -DnewVersion=$VERSION
./mvnw versions:commit
./mvnw versions:update-child-modules
./mvnw clean install -P deploy -DskipTests=true
./mvnw clean deploy -P deploy -DskipTests=true
